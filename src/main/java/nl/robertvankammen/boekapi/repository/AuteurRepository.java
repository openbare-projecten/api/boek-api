package nl.robertvankammen.boekapi.repository;

import nl.robertvankammen.boekapi.model.Auteur;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AuteurRepository extends CrudRepository<Auteur, Long> {
    Auteur getByNaam(String name);
}
