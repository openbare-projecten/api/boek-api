package nl.robertvankammen.boekapi.repository;

import nl.robertvankammen.boekapi.model.Boek;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface BoekRepository extends CrudRepository<Boek, Long> {
    Boek getByTitel(String title);

    List<Boek> findAllByOrderByIdAsc();
}
