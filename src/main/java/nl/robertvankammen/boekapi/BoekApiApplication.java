package nl.robertvankammen.boekapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BoekApiApplication {

    public static void main(String[] args) {
        SpringApplication.run(BoekApiApplication.class, args);
    }
}
