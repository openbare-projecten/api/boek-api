package nl.robertvankammen.boekapi.controller;

import nl.robertvankammen.boekapi.model.Auteur;
import nl.robertvankammen.boekapi.model.Boek;
import nl.robertvankammen.boekapi.repository.AuteurRepository;
import nl.robertvankammen.boekapi.repository.BoekRepository;
import org.apache.commons.lang3.StringUtils;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController
public class BoekEndPoint {
    private BoekRepository boekRepository;
    private AuteurRepository auteurRepository;

    public BoekEndPoint(BoekRepository boekRepository, AuteurRepository auteurRepository) {
        this.boekRepository = boekRepository;
        this.auteurRepository = auteurRepository;
    }

    @RequestMapping("/boeken")
    public Iterable<Boek> getAlleBoeken() {
        return boekRepository.findAllByOrderByIdAsc();
    }

    @PostMapping("/boeken")
    public Boek newBook(@RequestBody Boek boek) {
        if (boekRepository.getByTitel(boek.getTitel()) == null) {
            if (!StringUtils.isNotBlank(boek.getTitel())) {
                throw new IllegalArgumentException("boek.titel mag niet leeg zijn");
            }
            if (boek.getAuteursLijst() == null || boek.getAuteursLijst().isEmpty()) {
                throw new IllegalArgumentException("boek.auteursLijst mag niet leeg zijn");
            }
            boek = voegAuteurToeAanBoek(boek, boek.getAuteursLijst());
            StringUtils.isNotBlank(boek.getTitel());
            StringUtils.isNotBlank(boek.getLocatie());
            boekRepository.save(boek);
            return boek;
        }
        throw new IllegalArgumentException("Boek met titel: " + boek.getTitel() + " bestaat al, als je een boek wilt updaten kies dan de PUT methode");
    }

    @PutMapping("/boeken/{id}")
    public Boek updateBook(@PathVariable("id") Long id, @RequestBody Boek boek) {
        Optional<Boek> oldBoekOptional = boekRepository.findById(id);
        if (oldBoekOptional.isPresent()) {
            Boek oldBoek = oldBoekOptional.get();
            // todo misschien het hele updaten van auteur en titel er uit halen?
            if (!auteurLijstGelijk(oldBoek.getAuteursLijst(), boek.getAuteursLijst())) {
                updateAuteurLijst(oldBoek, boek);
            }
            boekRepository.save(boek);
        } else {
            throw new IllegalArgumentException("Een boek met " + id + " is niet gevonden");
        }
        return boek;
    }

    private boolean auteurLijstGelijk(List<Auteur> list1, List<Auteur> list2) {
        if (list1.size() != list2.size()) {
            return false;
        }
        for (Auteur auteur : list1) {
            if (list2.stream().map(Auteur::getId).noneMatch(id -> auteur.getId().equals(id))) {
                return false;
            }
        }
        return true;
    }

    private void updateAuteurLijst(Boek oldBoek, Boek newBoek) {
        oldBoek.getAuteursLijst().forEach(auteur -> {
            auteur.getBoekenLijst().remove(oldBoek);
            auteurRepository.save(auteur);
        });
        newBoek.getAuteursLijst().forEach(ath -> {
            Auteur auteur = auteurRepository.findById(ath.getId()).get();
            auteur.getBoekenLijst().add(newBoek);
            auteurRepository.save(auteur);
        });
    }

    private Boek voegAuteurToeAanBoek(Boek boek, List<Auteur> auteurList) {
        List<Auteur> auteurListFromDatabase = auteurList.stream()
                .map(Auteur::getId)
                .map(authId -> auteurRepository.findById(authId))
                .filter(Optional::isPresent)
                .map(Optional::get)
                .collect(Collectors.toList());
        if (auteurList.size() == auteurListFromDatabase.size()) {
            boek.getAuteursLijst().clear();
            for (Auteur auteur : auteurListFromDatabase) {
                boek = boekRepository.save(boek);
                auteur.getBoekenLijst().add(boek);
                auteurRepository.save(auteur);
                boek.getAuteursLijst().add(auteur);

            }
        } else {
            // TODO dit misschien anders zodat er ook komt te staan welke id's niet bestaan
            throw new IllegalArgumentException("Not all Authors exist");
        }
        return boek;
    }
}
