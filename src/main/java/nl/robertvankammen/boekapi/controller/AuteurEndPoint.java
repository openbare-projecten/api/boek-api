package nl.robertvankammen.boekapi.controller;

import nl.robertvankammen.boekapi.model.Auteur;
import nl.robertvankammen.boekapi.repository.AuteurRepository;
import org.apache.commons.lang3.StringUtils;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class AuteurEndPoint {
    private AuteurRepository auteurRepository;

    public AuteurEndPoint(AuteurRepository auteurRepository) {
        this.auteurRepository = auteurRepository;
    }

    @RequestMapping("/auteurs")
    public Iterable<Auteur> getAlleAuteurs() {
        return auteurRepository.findAll();
    }

    @PostMapping("/auteurs")
    public Auteur newAuthor(@RequestBody Auteur auteur) {
        if (auteurRepository.getByNaam(auteur.getNaam()) == null) {
            if (!StringUtils.isNotBlank(auteur.getNaam())) {
                throw new IllegalArgumentException("auteur.naam mag niet leeg zijn");
            }
            return auteurRepository.save(auteur);
        }
        throw new IllegalArgumentException("Auteur met naam: " + auteur.getNaam() + " bestaad al, gebruik de PUT methode als je deze auteur wil updaten");
    }
}
