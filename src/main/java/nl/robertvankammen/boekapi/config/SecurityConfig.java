package nl.robertvankammen.boekapi.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import static org.apache.commons.lang3.StringUtils.isBlank;

@EnableWebSecurity
@Configuration
public class SecurityConfig extends WebSecurityConfigurerAdapter implements WebMvcConfigurer {

    private final Environment environment;

    @Value("${gebruikersnaam:}")
    private String gebruikersnaam;

    @Value("${wachtwoord:}")
    private String wachtwoord;

    public SecurityConfig(Environment environment) {
        this.environment = environment;
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .cors().disable()
                .csrf().disable()
                .authorizeRequests()
                .anyRequest().authenticated().and()
                .httpBasic().and()
                .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);
    }

    @Autowired
    public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
        String gebruiker = gebruikersnaam;
        String wachtwoord = this.wachtwoord;

        if (isBlank(gebruiker)) {
            gebruiker = environment.getProperty("spring.security.user.name");
        }
        if (isBlank(wachtwoord)) {
            gebruiker = environment.getProperty("spring.security.user.password");
        }

        auth
                .inMemoryAuthentication()
                .withUser(gebruiker).password("{noop}" + wachtwoord).roles("USER");
    }

}
