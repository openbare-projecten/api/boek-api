package nl.robertvankammen.boekapi.model;

import javax.persistence.*;
import java.util.List;

@Entity
public class Boek {

    @Id
    @GeneratedValue
    private Long id;
    @Column(unique = true, nullable = false)
    private String titel;
    @ManyToMany
    private List<Auteur> auteursLijst;
    @Column(nullable = false)
    private String locatie;

    private String serie;
    private String serieDeel;
    private String categorie;

    public Long getId() {
        return id;
    }

    public String getTitel() {
        return titel;
    }

    public void setTitel(String titel) {
        this.titel = titel;
    }

    public List<Auteur> getAuteursLijst() {
        return auteursLijst;
    }

    public void setAuteursLijst(List<Auteur> auteursLijst) {
        this.auteursLijst = auteursLijst;
    }

    public String getLocatie() {
        return locatie;
    }

    public void setLocatie(String locatie) {
        this.locatie = locatie;
    }

    public String getSerieDeel() {
        return serieDeel;
    }

    public void setSerieDeel(String serieDeel) {
        this.serieDeel = serieDeel;
    }

    public String getSerie() {
        return serie;
    }

    public void setSerie(String serie) {
        this.serie = serie;
    }

    public String getCategorie() {
        return categorie;
    }

    public void setCategorie(String categorie) {
        this.categorie = categorie;
    }
}
