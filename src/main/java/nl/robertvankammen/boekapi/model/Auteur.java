package nl.robertvankammen.boekapi.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.List;

@Entity
public class Auteur {

    @Id
    @GeneratedValue
    private Long id;
    @Column(unique = true, nullable = false)
    private String naam;
    @ManyToMany
    @JsonIgnore
    private List<Boek> boekenLijst;

    public Long getId() {
        return id;
    }

    public String getNaam() {
        return naam;
    }

    public void setNaam(String naam) {
        this.naam = naam;
    }

    public List<Boek> getBoekenLijst() {
        return boekenLijst;
    }

    public void setBoekenLijst(List<Boek> boekenLijst) {
        this.boekenLijst = boekenLijst;
    }
}
